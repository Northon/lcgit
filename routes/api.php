<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->name('api.')->group(function(){
    Route::prefix('tasks')->group(function(){

        Route::get('/', 'TaskController@index')->name('index_tasks');
        Route::get('/{id}', 'TaskController@show')->name('single_tasks');

        Route::post('/', 'TaskController@store')->name('store_tasks');
        Route::put('/{id}', 'TaskController@update')->name('update_tasks');

        Route::delete('/{id}', 'TaskController@delete')->name('delete_tasks');
    });
});

