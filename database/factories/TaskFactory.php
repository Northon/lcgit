<?php

use Faker\Generator as Faker;

$factory->define(\App\Task::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'priority' => rand(1, 10),
        'description' => $faker->text,
    ];
});
