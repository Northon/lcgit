<?php

namespace App\Http\Controllers\Api;

use App\API\ApiError;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * @var task
     */
    private $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index()
    {
        return response()->json($this->task->paginate(10));
    }

    public function show($id)
    {
        $task = $this->task->find($id);

        if(! $task) return response()->json(ApiError::errorMessage('Tarefa não encontrada!', 4040), 404);

        $data = ['data' => $task];
        return response()->json($data);
    }

    public function store(Request $request)
    {
        try {

            $taskData = $request->all();
            $this->task->create($taskData);

            $return = ['data' => ['msg' => 'Tarefa criada com sucesso!']];
            return response()->json($return, 201);

        } catch (\Exception $e) {
            if(config('app.debug')) {
                return response()->json(ApiError::errorMessage($e->getMessage(), 1010), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar operação de salvar', 1010),  500);
        }
    }

    public function update(Request $request, $id)
    {
        try {

            $taskData = $request->all();
            $task     = $this->task->find($id);
            $task->update($taskData);

            $return = ['data' => ['msg' => 'Tarefa atualizada com sucesso!']];
            return response()->json($return, 201);

        } catch (\Exception $e) {
            if(config('app.debug')) {
                return response()->json(ApiError::errorMessage($e->getMessage(), 1011),  500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar operação de atualizar', 1011), 500);
        }
    }

    public function delete(task $id)
    {
        try {
            $id->delete();

            return response()->json(['data' => ['msg' => 'Tarefa: ' . $id->name . ' removida com sucesso!']], 200);

        }catch (\Exception $e) {
            if(config('app.debug')) {
                return response()->json(ApiError::errorMessage($e->getMessage(), 1012),  500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar operação de remover', 1012),  500);
        }
    }
}
